//
//  NSDictionary+Extensions.m
//  iG_Inspect
//
//  Created by Keith Dougherty on 2/16/17.
//  Copyright © 2017 Hollance. All rights reserved.
//

#import "NSDictionary+Extensions.h"

@implementation NSMutableDictionary (Extensions)

- (id)objectForCaseInsensitiveKey:(NSString *)key
{
    NSPredicate *predicate     = [NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings)
                                       {
                                           return [evaluatedObject compare:key options:NSCaseInsensitiveSearch] == NSOrderedSame;
                                       }];
    
    NSString *foundKey         = [self.allKeys filteredArrayUsingPredicate:predicate].firstObject;
    
    if (![NSString isNullOrEmpty:foundKey])
    {
        return [self objectForKey:foundKey];
    }
    
    return nil;
}

@end
