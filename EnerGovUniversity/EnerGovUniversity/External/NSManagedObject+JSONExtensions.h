//
//  NSManagedObject+JSONExtensions.h
//  IGCodeEnforcement
//
//  Created by Jonathan Fife on 5/9/12.
//  Copyright (c) 2012 Energov Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSManagedObject (JSONExtensions)

//Clone a managed object
- (void)setValueFromDictionary:(NSDictionary *)dict mapping:(NSDictionary *)mapping;

- (NSManagedObject *)cloneExludeEntities:(NSArray *)namesOfEntitiesToExclude;

- (NSManagedObject *)cloneExludeRelationship:(NSArray *)EntityRelationToExclude;

//Create json object to send to the server
-(NSDictionary*) createDictionaryWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeEntities:(NSArray *)namesOfEntitiesToExclude ;

-(NSDictionary*) createDictionary;

-(NSDictionary*) createDictionaryWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeRelationship:(NSArray *)EntityRelationToExclude;

//check whether a object is valid
//-(NSArray*) checkobjectIsValidExcludeRelationship:(NSArray*) EntityRelationToExclude ;

//Check whether a object is dirty
-(BOOL) ManagedObjectAndItsChildrenHasChangesExludeRelationship:(NSArray *)EntityRelationToExclude ;

@end
