/*!
 * \file NSDate+MHExtensions.h
 */

/*!
 * Useful extensions to the NSDate class.
 */
@interface NSDate (MHExtensions)

/*!
 * Converts an ASP.NET style JSON date (such as "/Date(1307624353893-0500)/")
 * into an NSDate object.
 */
+ (NSDate *) dateWithJsonDateString:(NSString*)dateString;

+ (NSDate*) localDateWithJsonDateString:(NSString*)stringDate;

- (NSString*) jsonDateStringFromDate;

- (NSString *) jsonDateStringFromUTCDateUsingSystemTime:(BOOL) useSystemTimeOffset;

+ (NSDate *) dateFromString:(NSString *) dateString;

+ (NSDate *) dateTimeFromString:(NSString *) dateString;

/*!
 * Adds the specified number of days to this date.
 */
- (NSDate*)dateByAddingDays:(NSInteger)numDays;

/*!
 * Subtracts the specified number of days from this date.
 */
- (NSDate*)dateBySubtractingDays:(NSInteger)numDays;

/*!
 * Returns the number of days between two dates.
 */
- (NSInteger)differenceInDaysTo:(NSDate*)toDate;

/*!
 * Strips the time components off of this date. The result is a new NSDate
 * at midnight in your local time zone.
 */
- (NSDate *)dateWithoutTime;


- (NSInteger)getDateYear;

- (NSString *) convertDate;

- (NSString *) convertDateTime;


+ (NSDate *) addBusinessDays:(NSInteger) days ToDate:(NSDate *) start ExcludingHolidays:(NSArray *) holidays;

@end
