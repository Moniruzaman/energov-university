
#import "MHFileUtil.h"

static NSString* documentsDirectory = nil;
static NSString* privateDirectory = nil;

NSString* MHPathInBundle(NSString* path)
{
	return [[NSBundle mainBundle] pathForResource:[path lastPathComponent] ofType:nil inDirectory:[path stringByDeletingLastPathComponent]];
}

NSString* MHPathInDocuments(NSString* filename)
{
	return [MHDocumentsDirectory() stringByAppendingPathComponent:filename];
}

NSString* MHPathInPrivateDocuments(NSString* filename)
{
	return [MHPrivateDocumentsDirectory() stringByAppendingPathComponent:filename];
}

NSURL* MHFileURLInDocuments(NSString* filename)
{
	return [NSURL fileURLWithPath:MHPathInDocuments(filename)];
}

NSURL* MHFileURLInPrivateDocuments(NSString* filename)
{
	return [NSURL fileURLWithPath:MHPathInPrivateDocuments(filename)];
}

NSString* MHDocumentsDirectory()
{
	if (documentsDirectory == nil)
	{
		NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		documentsDirectory = [[paths objectAtIndex:0] copy];
	}
	return documentsDirectory;
}

NSString* MHPrivateDocumentsDirectory()
{
	if (privateDirectory == nil)
	{
		NSArray* paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
		NSString* libraryDirectory = [paths objectAtIndex:0];
		privateDirectory = [[libraryDirectory stringByAppendingPathComponent:@"Private Documents"] copy];
		MHCreateDirectory(privateDirectory);
	}
	return privateDirectory;
}

NSString* MHTemporaryDirectory()
{
	return NSTemporaryDirectory();
}

void MHCreateDirectory(NSString* path)
{
	NSFileManager* fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:path])
	{
		NSError* error = nil;
		if (![fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error])
			NSLog(@"Error creating directory: %@", [error description]);
	}
}

void MHRemoveFile(NSString* path)
{
	NSFileManager* fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:path])
	{
		NSError* error = nil;
		if (![fileManager removeItemAtPath:path error:&error])
			NSLog(@"Error removing: %@", [error description]);
	}
}

void MHRemoveFromDocuments(NSString* path)
{
	MHRemoveFile(MHPathInDocuments(path));
}

void MHRemoveFromPrivateDocuments(NSString* path)
{
	MHRemoveFile(MHPathInPrivateDocuments(path));
}

BOOL MHFileExists(NSString* path)
{
	NSFileManager* fileManager = [NSFileManager defaultManager];
	return [fileManager fileExistsAtPath:path];
}

NSString* MHSanitizeFilename(NSString* filename)
{
	// There does not seem to be a convenience method for removing unwanted
	// characters from a string, so we have to do it the old fashioned way.
	NSMutableString* stripped = [NSMutableString stringWithCapacity:filename.length];
	for (int t = 0; t < filename.length; ++t)
	{
		unichar c = [filename characterAtIndex:t];

		// Only allow a-z, A-Z, 0-9, space, -
		if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') 
		||  (c >= '0' && c <= '9') || c == ' ' || c == '-')
			[stripped appendFormat:@"%c", c];
		else
			[stripped appendString:@"_"];

		// Alternative approach: allow all characters except : ? * < > " | / \ ;
		// This will leave names intact if they contain UTF-8 characters, but
		// this may not work very well when sharing the files with less-capable
		// file systems.
		//if (c != ':' && c != '?' && c != '<' && c != '>' && c != '"' 
		//&&  c != '|' && c != '/' && c != '\\' && c != ';')
		//	[stripped appendFormat:@"%c", c];
	}

	// No empty spaces at the beginning or end of the path name (also no dots
	// at the end); that messes up the Windows file system.
	return [stripped stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
