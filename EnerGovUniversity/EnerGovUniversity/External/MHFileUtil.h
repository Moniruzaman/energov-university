/*!
 * \file MHFileUtil.h
 *
 * Various utility functions for working with files and directories.
 */

/*! 
 * Returns the path to a file inside the bundle.
 *
 * This is useful for files that you add inside "folder references" in Xcode
 * (the blue folders). Unlike regular resources, these files are not stored in
 * the bundle's top-level directory but inside subfolders. To get the path to
 * such a file, do MHPathInBundle(@"Folder/File.ext").
 */
NSString* MHPathInBundle(NSString* path);

/*!
 * Returns the path to a file in the app's Documents directory.
 */
NSString* MHPathInDocuments(NSString* filename);

/*!
 * Returns the path to a file in the Private Documents directory.
 */
NSString* MHPathInPrivateDocuments(NSString* filename);

/*!
 * Returns the URL to a file in the app's Documents directory.
 */
NSURL* MHFileURLInDocuments(NSString* filename);

/*!
 * Returns the URL to a file in the Private Documents directory.
 */
NSURL* MHFileURLInPrivateDocuments(NSString* filename);

/*!
 * Returns the path of the app's Documents directory.
 */
NSString* MHDocumentsDirectory();

/*!
 * Returns the path to "app/Library/Private Documents", which is a good place
 * to store files you do not want to expose through iTunes file sharing.
 *
 * See also Technical Q&A QA1699.
 *
 * \note If the directory does not exist yet, it will be created.
 */
NSString* MHPrivateDocumentsDirectory();

/*!
 * Returns the path to app/tmp.
 */
NSString* MHTemporaryDirectory();

/*!
 * Creates the specified directory (and any parent directories) if it does not
 * exist yet.
 */
void MHCreateDirectory(NSString* path);

/*!
 * Removes a file or directory (and all its subdirectories). Does nothing if 
 * the specified file or directory does not exist.
 */
void MHRemoveFile(NSString* path);

/*!
 * Removes a file or directory from the app's Documents directory.
 */
void MHRemoveFromDocuments(NSString* path);

/*!
 * Removes a file or directory from the Private Documents directory.
 */
void MHRemoveFromPrivateDocuments(NSString* path);

/*!
 * Determines whether the file or directory exists at the specified path.
 */
BOOL MHFileExists(NSString* path);

/*!
 * Replaces everything that is not a-z, A-Z, 0-9, - or space with an underscore
 * and trims any whitespace off of the start and end. This makes sure the string
 * can be used as a filename, also on inferior operating systems.
 */
NSString* MHSanitizeFilename(NSString* filename);
