/*!
 * \file NSManagedObjectContext+MHExtensions.h
 */

#import <CoreData/CoreData.h>


/*!
 * Useful extensions to the NSManagedObjectContext class.
 */
@interface NSManagedObjectContext (Extensions)


+(NSManagedObject*)cloneAttributes:(NSManagedObject *)source;

+(NSManagedObject *) clone:(NSManagedObject *)source;


/*!
 * Convenience method that inserts a new entity into the context.
 */
- (id)insertNewEntityForName:(NSString*)name;

/*!
 * Saves the context. Returns NO if there is an error and broadcasts the
 * NSManagedObjectContextSaveDidFailNotification.
 */
- (BOOL)saveContext;

/*!
 * Returns a single object with the given entity name.
 */
- (id)objectForEntityName:(NSString*)entityName;

/*!
 * Returns a single object with the given entity name, limited by predicate.  
 * If nothing found, inserts new entity into context.
 */
- (id) objectForEntityName:(NSString*)entityName withPredicateString:(NSString *) string, ...;

- (id) objectForEntityName:(NSString*)entityName withPredicate:(NSPredicate *)predicate;

/*!
 * Fetches all objects with the given entity name, unsorted.
 */
- (NSArray*)allObjectsForEntityName:(NSString*)entityName;

/*!
 * Fetches all objects with the given entity name, sorted by a certain 
 * attribute.
 */
- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending;

/*!
 * Fetches all objects with the given entity name, limited by a predicate, 
 * unsorted.
 */
- (NSArray*)allObjectsForEntityName:(NSString*)entityName withPredicateString:(NSString*)string, ...;


- (NSArray*)allObjectsForEntityName:(NSString*)entityName withPredicate:(NSPredicate *)predicate;

/*!
 * Fetches all objects with the given entity name, limited by a predicate,
 * sorted by a certain attribute.
 */
- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending withPredicateString:(NSString*)string, ...;


- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending withPredicate:(NSPredicate *)predicate;

- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedByDesciptors:(NSArray *)sortDescriptors withPredicate:(NSPredicate *)predicate;

/*!
 * Deletes all objects with the given entity name.
 * 
 * This is a pretty dumb solution that may require multiple trips to the
 * database as nothing is prefetched, so it's not optimal for big datasets.
 */

- (void) deleteAllObjectsForEntityName:(NSString*)entityName withPredicate:(NSPredicate*) predicate;

- (void) deleteAllObjectsForEntityName:(NSString*)entityName;

- (void) deleteManagedObject:(NSManagedObject *)object;

- (void) deleteManagedObjects:(NSSet *) objects;

- (void) deleteOrderedManagedObjects:(NSOrderedSet *) objects;

- (void) deleteManagedObjectWithId:(NSManagedObjectID *) objectId;

@end
