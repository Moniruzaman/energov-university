
#import "NSDate+MHExtensions.h"

@implementation NSDate (MHExtensions)




+ (NSDate*) dateWithJsonDateString:(NSString*)stringDate
{
    if ([NSString isNullOrEmpty:stringDate])
        return nil;
    
    NSInteger ix0                   = [stringDate rangeOfString:@"("].location;
    NSInteger ix1                   = [stringDate rangeOfString:@")"].location;
    
    if(ix0 == NSNotFound || ix1 == NSNotFound)
        return nil;
    
    NSRange range                   = NSMakeRange(ix0 + 1, ix1 - ix0 - 1);
    NSString *dateString            = [stringDate substringWithRange:range];
    
    // WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision)
    // whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
    NSTimeInterval unixTime         = [dateString doubleValue] / 1000;
 
    NSDate *date                    = [NSDate dateWithTimeIntervalSince1970:unixTime];
 
    return date;
}

+ (NSDate*) localDateWithJsonDateString:(NSString*)stringDate
{
    if ([NSString isNullOrEmpty:stringDate])
        return nil;
    
    NSInteger ix0                   = [stringDate rangeOfString:@"("].location;
    NSInteger ix1                   = [stringDate rangeOfString:@")"].location;
    
    if(ix0 == NSNotFound || ix1 == NSNotFound)
        return nil;
    
    NSRange range                   = NSMakeRange(ix0 + 1, ix1 - ix0 - 1);
    NSString *dateString            = [stringDate substringWithRange:range];
    
    NSTimeZone *serverTimezone      = [NSTimeZone defaultTimeZone];
    NSTimeZone *localTimezone       = [NSTimeZone systemTimeZone];
    
    NSInteger serverTimeOffset      = [serverTimezone secondsFromGMT];
    NSInteger localTimeOffset       = [localTimezone  secondsFromGMT];
    
    NSInteger timezoneOffset        = serverTimeOffset - localTimeOffset;
    

    // WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision)
    // whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
    NSTimeInterval unixTime         = dateString.doubleValue / 1000;
    NSTimeInterval timeSince1970    = unixTime + timezoneOffset;
    
    NSDate *date                    = [NSDate dateWithTimeIntervalSince1970:timeSince1970];

    return date;
}

+ (NSDate *) dateFromString:(NSString *) dateString
{
    static NSDateFormatter* formatter = nil;
    
    if (formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyy-MM-dd"];
        [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    return [formatter dateFromString:dateString];
}


+ (NSDate *) dateTimeFromString:(NSString *) dateString
{
    static NSDateFormatter* formatter = nil;
    
    if (formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    return [formatter dateFromString:dateString];
}

- (NSString*) jsonDateStringFromDate
{
//    NSInteger LocalOffset           = [[NSTimeZone systemTimeZone]  secondsFromGMT]; //get number of seconds to add or subtract according to the client default time zone
//    NSInteger ServerOffset          = [[NSTimeZone defaultTimeZone] secondsFromGMT];
    
    
    //NSDate *date                    = [self dateByAddingTimeInterval:LocalOffset];
    
    return [NSString stringWithFormat:@"/Date(%lld000)/", (long long)([self timeIntervalSince1970])];
}

- (NSString *) jsonDateStringFromUTCDateUsingSystemTime:(BOOL) useSystemTimeOffset
{
    NSInteger UTCoffset = 0;
    
    if (useSystemTimeOffset)
    {
        UTCoffset       = [[NSTimeZone systemTimeZone] secondsFromGMT];
    }
    else
    {
        UTCoffset       = [[NSTimeZone defaultTimeZone] secondsFromGMT];
    }
    
    NSDate *newDate     = [self dateByAddingTimeInterval:UTCoffset];
    
    return [NSString stringWithFormat:@"/Date(%lld000)/", (long long)([newDate timeIntervalSince1970])];
}

- (NSDate*)dateByAddingDays:(NSInteger)numDays
{
	NSDateComponents* components    = [[NSDateComponents alloc] init];
	[components setDay:numDays];
	NSCalendar* gregorian           = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	NSDate* date                    = [gregorian dateByAddingComponents:components toDate:self options:0];
	return date;
}

- (NSDate*)dateBySubtractingDays:(NSInteger)numDays
{
    NSDateComponents* components    = [[NSDateComponents alloc] init];
    [components setDay:-numDays];
    NSCalendar* gregorian           = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate* date                    = [gregorian dateByAddingComponents:components toDate:self options:0];
    return date;
}



- (NSInteger)differenceInDaysTo:(NSDate*)toDate
{
	NSDate* toDateWithoutTime       = [toDate dateWithoutTime];
	NSDate* fromDateWithoutTime     = [self dateWithoutTime];

	NSCalendar* gregorian           = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	NSDateComponents* components    = [gregorian components:NSCalendarUnitDay fromDate:fromDateWithoutTime toDate:toDateWithoutTime options:0];
	NSInteger days                  = [components day];
	return days;
}

- (NSDate*)dateWithoutTime
{
    NSDate *startOfDay              = nil;
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&startOfDay interval:NULL forDate:self];
    return startOfDay;
}

- (NSInteger)getDateYear
{
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:self];
    return [components year];
}

- (NSString *) convertDate
{
    static NSDateFormatter* formatter       = nil;
    
    if (formatter == nil)
    {
        formatter                           = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [formatter setDateFormat:@"MM-dd-yyyy"];
    }
    return [formatter stringFromDate:self];
}

- (NSString *) convertDateTime
{
    static NSDateFormatter* formatter = nil;
    
    if (formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
        [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    return [formatter stringFromDate:self];
}


+ (NSDate *) addBusinessDays:(NSInteger) days ToDate:(NSDate *) start ExcludingHolidays:(NSArray *) holidays
{
    NSDate * end = start;
    
    for (int i = 0; i < days; i++)
    {
        // If the current date is a weekend or holiday, advance:
        end = [NSDate ensureDateIsWeekday:end andNonHoliday:holidays];
        
        // And move the date forward by one day:
        end = [NSDate addDays:1 ToDate:end];
    }
    
    // Make sure we didn't end on a weekend or holiday:
    end = [NSDate ensureDateIsWeekday:end andNonHoliday:holidays];
    
    return end;
}

#pragma mark - Private Methods

+ (NSDate *) ensureDateIsWeekday:(NSDate *) date andNonHoliday:(NSArray *) holidays
{
    // If the current date is either a holiday or a weekend
    while (![self isWeekday:date])// || [self isDate:date aHoliday:holidays])
    {
        // Add one day to the date:
        date = [self addDays:1 ToDate:date];
    }
    
    return date;
}

+ (BOOL) isWeekday:(NSDate *) date
{
    NSUInteger day = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:date] weekday];//NSWeekdayCalendarUnit deprecated
    
    const int kSunday = 1;
    const int kSaturday = 7;
    
    BOOL isWeekdayResult = day != kSunday && day != kSaturday;
    
    return isWeekdayResult;
}

//+ (BOOL) isDate:(NSDate *) date aHoliday:(NSArray* ) holidays
//{
//    BOOL isHolidayResult = NO;
//    
//    const unsigned kUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;// NSYearCalendarUnit,NSMonthCalendarUnit,NSDayCalendarUnit deprecated
//    NSDateComponents * components = [[NSCalendar currentCalendar] components:kUnits fromDate:date];
//    
//    for (int i = 0; i < [holidays count]; i++)
//    {
//        HolidaySetupData * objHoliday = [holidays objectAtIndex:i];
//        NSDateComponents * holidayDateComponents = [[NSCalendar currentCalendar] components:kUnits fromDate:objHoliday.holidayDate];
//        
//        if ([components year] == [holidayDateComponents year]
//            && [components month] == [holidayDateComponents month]
//            && [components day] == [holidayDateComponents day])
//        {
//            isHolidayResult = YES;
//            break;
//        }
//    }
//    
//    return isHolidayResult;
//}

+ (NSDate *) addDays:(NSInteger) days ToDate:(NSDate *) date
{
    NSDateComponents * components = [[NSDateComponents alloc] init];
    [components setDay:days];
    
    NSDate * result = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:date options:0];
    
    return result;
}
@end
