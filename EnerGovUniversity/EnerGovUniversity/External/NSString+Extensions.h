/*!
 * \file NSString+Extensions.h
 */

/*!
 * Useful extensions to the NSString class.
 */
@interface NSString (Extensions)

/*!
 * Removes whitespace from the front and back of the string.
 */
- (NSString * _Nonnull) stringByTrimmingWhitespace;

/*!
 * Counts the number of lines in this string.
 */
- (NSInteger) countLines;

+(BOOL) isNullOrEmpty:(NSObject  * _Nullable ) object ;

+ (NSString *)stringWithGUID;

-(BOOL) containsString:(NSString * _Nonnull) subString;

+ (BOOL)isNumeric:(NSString * _Nonnull)string;

@end
