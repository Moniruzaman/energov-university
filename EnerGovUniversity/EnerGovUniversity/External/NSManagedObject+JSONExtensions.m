//
//  NSManagedObject+JSONExtensions.m
//  IGCodeEnforcement
//
//  Created by Jonathan Fife on 5/9/12.
//  Copyright (c) 2012 Energov Solutions. All rights reserved.
//

#import "NSManagedObject+JSONExtensions.h"
#import "NSManagedObjectContext+Extensions.h"
#import "NSDate+MHExtensions.h"
#import <objc/message.h>

@implementation NSManagedObject (JSONExtensions)

-(Class)classOfPropertyNamed:(NSString*)propertyName
{
    // Get Class of property to be populated.
    Class propertyClass = nil;
    objc_property_t property = class_getProperty([self class], [propertyName UTF8String]);
    NSString *propertyAttributes = [NSString stringWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
    NSArray *splitPropertyAttributes = [propertyAttributes componentsSeparatedByString:@","];
    if (splitPropertyAttributes.count > 0)
    {
        // xcdoc://ios//library/prerelease/ios/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
        NSString *encodeType = splitPropertyAttributes[0];
        NSArray *splitEncodeType = [encodeType componentsSeparatedByString:@"\""];
        NSString *className = splitEncodeType[1];
        propertyClass = NSClassFromString(className);
    }
    return propertyClass;
}

-(NSDictionary*) createDictionary
{
    return [self createDictionaryWithCopiedCache:[NSMutableDictionary dictionary]
                                  exludeEntities:[NSArray array]];
}

//Form an NSDictionary object based on its property name
-(NSDictionary *) createDictionaryWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeEntities:(NSArray *)namesOfEntitiesToExclude
{

    NSManagedObjectContext *managedObjectContext    = DATA_MODEL_CONTEXT.newBackgroundContext;
    
    NSRange range                                   = NSMakeRange(0, 1);
    NSString *entityName                            = [[self entity] name];
    
    if ([namesOfEntitiesToExclude containsObject:entityName])
    {
        return nil;
    }
    
    NSDictionary *cloned                            = [alreadyCopied objectForKey:[self objectID]];
   
    if (cloned != nil)
    {
        return cloned;
    }
    
    NSMutableDictionary* result                     = [[NSMutableDictionary alloc] initWithCapacity:20];
    
    [alreadyCopied setObject:result forKey:[self objectID]];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes                        = [[NSEntityDescription entityForName:entityName
                                                                   inManagedObjectContext:managedObjectContext] attributesByName];
    
    
    //loop through all attributes and assign then to the dictionary
    for (NSString *attr in attributes)
    {
        id value                                    = [self valueForKey:attr];
        
        //Convert nil value to [NSNull null] object
        if (value == nil)
        {
            value                                   = @"";
        }
        //Convert NSDate value to NSString
        else if([value isKindOfClass:[NSDate class]] && value !=nil)
        {
            value                                   = [(NSDate*)value jsonDateStringFromDate];
        }
        
        NSString* lowerCaseFirstCharacter           = [[(NSString*)attr substringToIndex:1] uppercaseString];
        NSString* mapKey                            = [(NSString*)attr stringByReplacingCharactersInRange:range
                                                                                               withString:lowerCaseFirstCharacter];
        
        [result setValue:value forKey:mapKey];
        
#if defined  (TESTING)
        //NSLog([NSString stringWithFormat:@"%@, %@", attr, NSStringFromClass([value class])]);
#endif
        
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships                     = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] relationshipsByName];
    
    for (NSString *relName in [relationships allKeys])
    {
        NSRelationshipDescription *rel              = [relationships objectForKey:relName];
        
        NSString *keyName = rel.name;
        
        NSString* lowerCaseFirstCharacter           = [[(NSString*)keyName substringToIndex:1] uppercaseString];
        
        NSString* mapKey                            = [(NSString*)keyName stringByReplacingCharactersInRange:range withString:lowerCaseFirstCharacter];
        
        if ([rel isToMany])
        {
            if (rel.isOrdered)
            {
                //get a set of all objects in the relationship
                NSMutableOrderedSet *sourceSet          = [self mutableOrderedSetValueForKey:keyName];
                NSMutableArray *copiedArray             = [[NSMutableArray alloc] initWithCapacity:20];
                NSEnumerator *e                         = [sourceSet objectEnumerator];
                
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSDictionary* copiedDict            = [relatedObject createDictionaryWithCopiedCache:alreadyCopied exludeEntities:namesOfEntitiesToExclude];
                    [copiedArray addObject:copiedDict];
                }
                
                [result setValue:copiedArray forKey:mapKey];
            }
            else
            {
                //get a set of all objects in the relationship
                NSMutableSet *sourceSet                 = [self mutableSetValueForKey:keyName];
                NSMutableArray *copiedArray             = [[NSMutableArray alloc] initWithCapacity:20];
                NSEnumerator *e                         = [sourceSet objectEnumerator];
                
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSDictionary* copiedDict            = [relatedObject createDictionaryWithCopiedCache:alreadyCopied exludeEntities:namesOfEntitiesToExclude];
                    [copiedArray addObject:copiedDict];
                }
                
                [result setValue:copiedArray forKey:mapKey];
            }

        }
        else
        {
            NSManagedObject *relatedObject = [self valueForKey:keyName];
            
            if (relatedObject != nil)
            {
                NSDictionary* copiedDict = [relatedObject createDictionaryWithCopiedCache:alreadyCopied  exludeEntities:namesOfEntitiesToExclude];
                [result setValue:copiedDict forKey:mapKey];
            }
        }
    }
    
    return (NSDictionary*) result;
    
}

-(BOOL) ManagedObjectAndItsChildrenHasChangesExludeRelationship:(NSArray *)EntityRelationToExclude
{
    NSManagedObjectContext *managedObjectContext    = DATA_MODEL_CONTEXT.newBackgroundContext;
    
    BOOL result                                     = NO;
    
    NSString *entityName                            = [[self entity] name];
    
    @try
    {
        if([self hasChanges])
        {
            return result                           = YES;
        }
        
        //Loop through all relationships and validates them  
        NSDictionary *relationships                 = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] relationshipsByName];
        
        for (NSString *relName in [relationships allKeys])
        {
            NSRelationshipDescription *rel          = [relationships objectForKey:relName];
            
            NSString *keyName                       = rel.name;
            
            BOOL shouldBeExclude                    = NO;
            
            //Check whether this relationship should be excluede
            for(int i=0; i < [EntityRelationToExclude count]; i = i+2)
            {
                NSString* excludeKey                = [EntityRelationToExclude objectAtIndex:i];
                NSString* excludeValue              = [EntityRelationToExclude objectAtIndex:i+1];
                
                if([excludeKey isEqualToString:keyName] && [entityName isEqualToString:excludeValue])
                {
                    shouldBeExclude                 = YES;
                    break;
                }
            }
            if ([rel isToMany])
            {
                if(!shouldBeExclude)
                {
                    if (rel.isOrdered)
                    {
                        //get a set of all objects in the relationship
                        NSMutableOrderedSet *sourceSet  = [self mutableOrderedSetValueForKey:keyName];
                        //NSMutableArray *copiedArray = [[NSMutableArray alloc] initWithCapacity:20];
                        NSEnumerator *e                 = [sourceSet objectEnumerator];
                        NSManagedObject *relatedObject;
                        
                        while ( relatedObject = [e nextObject])
                        {
                            BOOL childResult            = [relatedObject hasChanges];
                            
                            if(childResult)
                            {
                                return result = YES;
                            }
                        }

                    }
                    else
                    {
                        //get a set of all objects in the relationship
                        NSMutableSet *sourceSet         = [self mutableSetValueForKey:keyName];
                        //NSMutableArray *copiedArray = [[NSMutableArray alloc] initWithCapacity:20];
                        NSEnumerator *e                 = [sourceSet objectEnumerator];
                        NSManagedObject *relatedObject;
                        
                        while ( relatedObject = [e nextObject])
                        {
                            BOOL childResult            = [relatedObject hasChanges];
                            
                            if(childResult)
                            {
                                return result = YES;
                            }
                        }
   
                    }
                }
            }
            else
            {
                if(!shouldBeExclude)
                {
                    NSManagedObject *relatedObject = [self valueForKey:keyName];
                   
                    if (relatedObject != nil)
                    {
                        BOOL childResult = [relatedObject hasChanges];
                       
                        if(childResult)
                        {
                            return result =YES;
                        
                        }
                        
                    }
                }
            }
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    return result;

}

//Form an NSDictionary object based on its property name
-(NSDictionary*) createDictionaryWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeRelationship:(NSArray *)EntityRelationToExclude
{
    NSManagedObjectContext *managedObjectContext    = DATA_MODEL_CONTEXT.newBackgroundContext;
    
    NSRange range                   = NSMakeRange(0, 1);
    NSString *entityName            = [[self entity] name];
    
    NSDictionary *cloned            = [alreadyCopied objectForKey:[self objectID]];
    if (cloned != nil)
    {
        return cloned;
    }
    NSMutableDictionary* result     = [[NSMutableDictionary alloc] initWithCapacity:20];
    [alreadyCopied setObject:result forKey:[self objectID]];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes        = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] attributesByName];
    

    //loop through all attributes and assign then to the dictionary
    for (NSString *attr in attributes)
    {
        id value = [self valueForKey:attr];
        
        //Convert nil value to [NSNull null] object
        if (value != nil)
        {
            //Convert NSDate value to NSString
            if([value isKindOfClass:[NSDate class]] && value !=nil)
            {
                value = [(NSDate*)value jsonDateStringFromDate];
            }
            
            NSString* lowerCaseFirstCharacter   = [[(NSString*)attr substringToIndex:1] uppercaseString];
            NSString* mapKey                    = [(NSString*)attr stringByReplacingCharactersInRange:range withString:lowerCaseFirstCharacter];
            
            [result setValue:value forKey:mapKey];
        }
        
#if defined  (TESTING)
        //NSLog([NSString stringWithFormat:@"%@, %@", attr, NSStringFromClass([value class])]);
#endif
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships             = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] relationshipsByName];
    
    for (NSString *relName in [relationships allKeys])
    {
        NSRelationshipDescription *rel      = [relationships objectForKey:relName];
        
        NSString *keyName                   = rel.name;
        
        NSString* lowerCaseFirstCharacter   = [[(NSString*)keyName substringToIndex:1] uppercaseString];
        NSString* mapKey                    = [(NSString*)keyName stringByReplacingCharactersInRange:range withString:lowerCaseFirstCharacter];
        
        BOOL shouldBeExclude                = NO;
        
        //Check whether this relationship should be excluede
        for(int i=0; i < [EntityRelationToExclude count]; i = i+2)
        {
            NSString* excludeKey            = [EntityRelationToExclude objectAtIndex:i];
            NSString* excludeValue          = [EntityRelationToExclude objectAtIndex:i+1];
            
            if([excludeKey isEqualToString:keyName] && [entityName isEqualToString:excludeValue])
            {
                shouldBeExclude = YES;
                break;
            }
        }

        //NSLog( [NSStringstringWithFormat:@"%@,%@", entityName, keyName] );
        if ([rel isToMany])
        {
            if(!shouldBeExclude)
            {
                if (rel.isOrdered)
                {
                    //get a set of all objects in the relationship
                    NSMutableOrderedSet *sourceSet      = [self mutableOrderedSetValueForKey:keyName];
                    NSMutableArray *copiedArray         = [[NSMutableArray alloc] initWithCapacity:20];
                    NSEnumerator *e                     = [sourceSet objectEnumerator];
                    
                    NSManagedObject *relatedObject;
                    
                    while ( relatedObject               = [e nextObject])
                    {
                        
                        NSDictionary* copiedDict        = [relatedObject createDictionaryWithCopiedCache:alreadyCopied exludeRelationship:EntityRelationToExclude];
                        [copiedArray addObject:copiedDict];
                    }
                    
                    [result setValue:copiedArray forKey:mapKey];
                }
                else
                {
                    //get a set of all objects in the relationship
                    NSMutableSet *sourceSet             = [self mutableSetValueForKey:keyName];
                    NSMutableArray *copiedArray         = [[NSMutableArray alloc] initWithCapacity:20];
                    NSEnumerator *e                     = [sourceSet objectEnumerator];
                    
                    NSManagedObject *relatedObject;
                    
                    while ( relatedObject = [e nextObject])
                    {
                        
                        NSDictionary* copiedDict        = [relatedObject createDictionaryWithCopiedCache:alreadyCopied exludeRelationship:EntityRelationToExclude];
                        [copiedArray addObject:copiedDict];
                    }
                    
                    [result setValue:copiedArray forKey:mapKey];
                }

            }
        }else
        {
            if(!shouldBeExclude)
            {
                NSManagedObject *relatedObject = [self valueForKey:keyName];
                if (relatedObject != nil)
                {
                    NSDictionary* copiedDict = [relatedObject createDictionaryWithCopiedCache:alreadyCopied  exludeRelationship:EntityRelationToExclude];
                    [result setValue:copiedDict forKey:mapKey];
                }
                else
                {
                    [result setValue:[NSNull null] forKey:mapKey];
                }
            }

        }
    }
    
    return (NSDictionary*) result;
    
}

- (void)setValueFromDictionary:(NSDictionary *)dict mapping:(NSDictionary *)mapping
{
    NSRange range = NSMakeRange(0, 1);
    
    if (mapping == nil)
    {
        //mapping only based on the dict key, lower the first character of the key string
        [dict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
        {
            if ([(NSObject*)key isKindOfClass:[NSString class]])
            {
                NSString* lowerCaseFirstCharacter   = [[(NSString*)key substringToIndex:1] lowercaseString];
                NSString* mapKey                    = [(NSString*)key stringByReplacingCharactersInRange:range withString:lowerCaseFirstCharacter];
                
                //Obj set values directly
                @try
                {
                    if([obj isKindOfClass:[NSNull class]])
                    {
                        if (class_getProperty([self class], [mapKey UTF8String]))
                        {
                            [self setValue:nil forKey:mapKey];
                        }
                    }
                    else if ([obj isKindOfClass:[NSString class]])
                    {
                        /*
                         * First, we need to check to see if the "mapKey" is a property of the object, or it'll crash
                         * Then we need to check the property's return type. If the type is NSDate, then we ignore it
                         * because the date will be set when the "parseDictionary" is called. Here, we ignore it.
                         * We're not using the "isKindOfClass" because Apple advised agaisnt using it for this kindof purpose.
                         * The best bet is to to string comparison.
                         */
                        if (class_getProperty([self class], [mapKey UTF8String]))
                        {
                            id className = [self classOfPropertyNamed:mapKey];
                            if (![[className description] isEqualToString:[NSDate description]])
                            {
                                [self setValue:obj forKey:mapKey];
                            }
                        }
                    }
                    else if ([obj isKindOfClass:[NSNumber class]])
                    {
                        if (class_getProperty([self class], [mapKey UTF8String]))
                        {
                            [self setValue:obj forKey:mapKey];
                        }
                    }
//                    else if ([obj isKindOfClass:[NSDictionary class]])
//                    {
//                        id property = [self valueForKey:mapKey];
//                        
//                        property = [DATA_MODEL_CONTEXT insertNewEntityForName:key];
//                        
//                        [(NSManagedObject*) property setValueFromDictionary:obj mapping:nil];
//                    }
                    //Do nothing, you need to implement this method in parse dictionary
                    //else if( [obj isKindOfClass:[NSArray class]]) {
                    //}
                }
                @catch (NSException *exception)
                {
                   // NSLog(@"%@", exception);
                }
                @finally
                {
                    
                }

            }
        }];
    }
}

- (NSManagedObject *)cloneExludeRelationship:(NSArray *)EntityRelationToExclude
{
    return [self cloneWithCopiedCache:[NSMutableDictionary dictionary] exludeRelationship:EntityRelationToExclude];
}
- (NSManagedObject *)cloneWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeRelationship:(NSArray *)EntityRelationToExclude
{
    NSString *entityName = [[self entity] name];
    
    NSManagedObject *cloned = [alreadyCopied objectForKey:[self objectID]];
    
    if (cloned != nil)
    {
        return cloned;
    }
    NSManagedObjectContext *managedObjectContext    = DATA_MODEL_CONTEXT.newBackgroundContext;
    
    //create new object in data store
    cloned =  [managedObjectContext insertNewEntityForName:entityName];
    
    [alreadyCopied setObject:cloned forKey:[self objectID]];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] attributesByName];
    
    for (NSString *attr in attributes)
    {
        [cloned setValue:[self valueForKey:attr] forKey:attr];
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] relationshipsByName];
   
    for (NSString *relName in [relationships allKeys])
    {
        
        NSRelationshipDescription *rel = [relationships objectForKey:relName];
        
        NSString *keyName = rel.name;
        
        BOOL shouldBeExclude = NO;
        //Check whether this relationship should be excluede
        
        for(int i=0; i < [EntityRelationToExclude count]; i = i+2)
        {
            NSString* excludeKey = [EntityRelationToExclude objectAtIndex:i];
            NSString* excludeValue = [EntityRelationToExclude objectAtIndex:i+1];
            
            if([excludeKey isEqualToString:keyName] && [entityName isEqualToString:excludeValue])
            {
                shouldBeExclude = YES;
                break;
            }
        }
        if ([rel isToMany])
        {
            if(!shouldBeExclude)
            {
                if (rel.isOrdered)
                {
                    //get a set of all objects in the relationship
                    NSMutableOrderedSet *sourceSet  = [self mutableOrderedSetValueForKey:keyName];
                    NSMutableOrderedSet *clonedSet  = [[NSMutableOrderedSet alloc] initWithCapacity:20];
                    NSEnumerator *e                 = [sourceSet objectEnumerator];
                    NSManagedObject *relatedObject;
                    
                    while ( relatedObject = [e nextObject])
                    {
                        //Clone it, and add clone to set
                        NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeRelationship:EntityRelationToExclude];
                        [clonedSet addObject:clonedRelatedObject];
                    }
                    [cloned setValue:clonedSet forKey:keyName];
                    
                }
                else
                {
                    //get a set of all objects in the relationship
                    NSMutableSet *sourceSet         = [self mutableSetValueForKey:keyName];
                    NSMutableSet *clonedSet         = [[NSMutableSet alloc] initWithCapacity:20];
                    NSEnumerator *e                 = [sourceSet objectEnumerator];
                    NSManagedObject *relatedObject;
                    
                    while ( relatedObject = [e nextObject])
                    {
                        //Clone it, and add clone to set
                        NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeRelationship:EntityRelationToExclude];
                        [clonedSet addObject:clonedRelatedObject];
                    }
                    [cloned setValue:clonedSet forKey:keyName];
                    
                }
  
            }
            else
            {
                
            }
        }
        else
        {
            if(!shouldBeExclude)
            {
                NSManagedObject *relatedObject = [self valueForKey:keyName];
               
                if (relatedObject != nil)
                {
                    NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeRelationship:EntityRelationToExclude];
                    [cloned setValue:clonedRelatedObject forKey:keyName];
                }
            }
        }
    }
    
    return cloned;

}

- (NSManagedObject *)cloneExludeEntities:(NSArray *)namesOfEntitiesToExclude
{
    return [self cloneWithCopiedCache:[NSMutableDictionary dictionary] exludeEntities:namesOfEntitiesToExclude];
}

//Only works when namesOfEntitiesToExclude = [NSArray array]
- (NSManagedObject *)cloneWithCopiedCache:(NSMutableDictionary *)alreadyCopied exludeEntities:(NSArray *)namesOfEntitiesToExclude
{
    NSString *entityName = [[self entity] name];
    
    if ([namesOfEntitiesToExclude containsObject:entityName])
    {
        return nil;
    }
    
    NSManagedObject *cloned = [alreadyCopied objectForKey:[self objectID]];
    
    if (cloned != nil)
    {
        return cloned;
    }
    
    NSManagedObjectContext *managedObjectContext    = DATA_MODEL_CONTEXT.newBackgroundContext;
    
    //create new object in data store
    cloned = [managedObjectContext insertNewEntityForName:entityName];
    
    [alreadyCopied setObject:cloned forKey:[self objectID]];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] attributesByName];
    
    for (NSString *attr in attributes) {
        [cloned setValue:[self valueForKey:attr] forKey:attr];
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships = [[NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext] relationshipsByName];
    for (NSString *relName in [relationships allKeys]){
        NSRelationshipDescription *rel = [relationships objectForKey:relName];
        
        NSString *keyName = rel.name;
        if ([rel isToMany])
        {
            if (rel.isOrdered)
            {
                //get a set of all objects in the relationship
                NSMutableOrderedSet *sourceSet  = [self mutableOrderedSetValueForKey:keyName];
                NSMutableOrderedSet *clonedSet  = [cloned mutableOrderedSetValueForKey:keyName];
                NSEnumerator *e                 = [sourceSet objectEnumerator];
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeEntities:namesOfEntitiesToExclude];
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
            else
            {
                //get a set of all objects in the relationship
                NSMutableSet *sourceSet         = [self mutableSetValueForKey:keyName];
                NSMutableSet *clonedSet         = [cloned mutableSetValueForKey:keyName];
                NSEnumerator *e                 = [sourceSet objectEnumerator];
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeEntities:namesOfEntitiesToExclude];
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
           
        }
        else
        {
            NSManagedObject *relatedObject = [self valueForKey:keyName];
            if (relatedObject != nil)
            {
                NSManagedObject *clonedRelatedObject = [relatedObject cloneWithCopiedCache:alreadyCopied exludeEntities:namesOfEntitiesToExclude];
                [cloned setValue:clonedRelatedObject forKey:keyName];
            }
        }
    }
    
    return cloned;
}

@end
