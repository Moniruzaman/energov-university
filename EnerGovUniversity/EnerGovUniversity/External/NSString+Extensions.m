
#import "NSString+Extensions.h"

@implementation NSString (Extensions)

+(BOOL) isNullOrEmpty:(NSObject *)object
{
    //If the object passed in is nil
    if(object == nil)
        return YES;
    
    //If the object passed in is type of class NSNull
    else if ([object isKindOfClass:[NSNull class]])
        return YES;
    
    //if object passed in is not a string
    else if (![object isKindOfClass:[NSString class]])
    {
        return YES;
    }
    //If object passed in is in fact a string
    else
    {
        //Remove any and all blank space from the string
        NSString *string = [(NSString *)object stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        
        //Check to make sure the string is not empty, or contains a null string
        if ([string isEqualToString:@""] || [[string lowercaseString] isEqualToString:@"<null>"] || [[string lowercaseString] isEqualToString:@"(null)"])
            return YES;
        else
            return NO;
    }
    return NO;
}

+ (NSString *)stringWithGUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

- (NSString*)stringByTrimmingWhitespace
{
    if (![NSString isNullOrEmpty:self])
        return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    else
        return @"";
}

- (NSInteger) countLines
{
    if (![NSString isNullOrEmpty:self])
        return [[self componentsSeparatedByString:@"\n"] count];
    else
        return 0;
}

- (BOOL) containsString:(NSString *) string
                options:(NSStringCompareOptions) options
{
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

-(BOOL) containsString:(NSString *) string
{
    return [self containsString:string options:NSCaseInsensitiveSearch];
}


+ (BOOL)isNumeric:(NSString *)newString
{
    BOOL onlyNumber = NO;
    
    NSCharacterSet* notDigits = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    if ([newString rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        onlyNumber = YES;
    }
    
    return onlyNumber;
}

@end
