
#import "NSManagedObjectContext+Extensions.h"

NSString* const NSManagedObjectContextSaveDidFailNotification = @"NSManagedObjectContextSaveDidFailNotification";

@implementation NSManagedObjectContext (Extensions)

- (id)insertNewEntityForName:(NSString*)name
{
    @synchronized(self)
    {
        return [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:self];
    }
}

- (BOOL)saveContext
{
    @try
    {
        NSError* error;
        
        if (![self save:&error])
        {
            NSLog(@"Error saving context %@, %@", error, [error userInfo]);
            
            return NO;
        }
        
        return YES;
    }
    @catch (NSException* exception)
    {
        NSLog(@"Exception while saving context %@, %@, %@", [exception name], [exception reason], [exception userInfo]);
        
        return NO;
    }
}

- (id) objectForEntityName:(NSString*)entityName
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity     = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest    = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSError* error                  = nil;
            NSArray* foundObjects           = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);

                return nil;
            }
            else if ([foundObjects count] == 0)
            {
                return nil;
            }
            else
            {
                return [foundObjects lastObject];
            }
        }
        @catch (NSException *exception)
        {
            return nil;
        }
    }
}

- (id) objectForEntityName:(NSString*)entityName withPredicateString:(NSString *) string, ...
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity     = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest    = [[NSFetchRequest alloc] init];
            
            [fetchRequest setEntity:entity];
            
            va_list variadicArguments;
            va_start(variadicArguments, string);
            
            NSPredicate* predicate          = [NSPredicate predicateWithFormat:string arguments:variadicArguments];
            
            va_end(variadicArguments);
            [fetchRequest setPredicate:predicate];
            
            NSError* error                  = nil;
            NSArray* foundObjects           = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
                return [self insertNewEntityForName:entityName];
            }
            else if ([foundObjects count] == 0)
            {
                return [self insertNewEntityForName:entityName];
            }
            else
            {
                return [foundObjects lastObject];
            }
        }
        @catch (NSException *exception)
        {
            return [self insertNewEntityForName:entityName];
        }
    }
}

- (id) objectForEntityName:(NSString*)entityName withPredicate:(NSPredicate *)predicate
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity     = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest    = [[NSFetchRequest alloc] init];
            
            [fetchRequest setEntity:entity];
            [fetchRequest setPredicate:predicate];
            
            NSError* error                  = nil;
            NSArray* foundObjects           = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);

                return [self insertNewEntityForName:entityName];
            }
            else if ([foundObjects count] == 0)
            {
                return [self insertNewEntityForName:entityName];
            }
            else
            {
                return [foundObjects lastObject];
            }
        }
        @catch (NSException *exception)
        {
            return [self insertNewEntityForName:entityName];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSError* error = nil;
            NSArray* foundObjects = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:ascending];
            [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            
            NSError* error = nil;
            NSArray* foundObjects = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
               // [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName withPredicateString:(NSString*)string, ...
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            va_list variadicArguments;
            va_start(variadicArguments, string);
            NSPredicate* predicate = [NSPredicate predicateWithFormat:string arguments:variadicArguments];
            va_end(variadicArguments);
            [fetchRequest setPredicate:predicate];
            
            NSError* error = nil;
            NSArray* foundObjects = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
               // [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}


- (NSArray*)allObjectsForEntityName:(NSString*)entityName withPredicate:(NSPredicate *)predicate
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity         = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest        = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            [fetchRequest setPredicate:predicate];
            
            NSError* error                      = nil;
            NSArray* foundObjects               = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
              //  [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending withPredicateString:(NSString*)string, ...
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity         = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest        = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSSortDescriptor* sortDescriptor    = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:ascending];
            [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            
            va_list variadicArguments;
            va_start(variadicArguments, string);
            NSPredicate* predicate              = [NSPredicate predicateWithFormat:string arguments:variadicArguments];
            va_end(variadicArguments);
            [fetchRequest setPredicate:predicate];
            
            NSError* error                      = nil;
            NSArray* foundObjects               = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
               // [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedBy:(NSString*)sortKey ascending:(BOOL)ascending withPredicate:(NSPredicate *)predicate
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity         = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest        = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSSortDescriptor* sortDescriptor    = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:ascending];
            
            [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            
            [fetchRequest setPredicate:predicate];
            
            NSError* error                      = nil;
            NSArray* foundObjects               = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
              //  [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }
    }
}

- (NSArray*)allObjectsForEntityName:(NSString*)entityName sortedByDesciptors:(NSArray *)sortDescriptors withPredicate:(NSPredicate *)predicate
{
    @synchronized(self)
    {
        @try
        {
            NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
            
            NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            [fetchRequest setSortDescriptors:sortDescriptors];
            
            [fetchRequest setPredicate:predicate];
            
            NSError* error = nil;
            NSArray* foundObjects = [self executeFetchRequest:fetchRequest error:&error];
            
            if (foundObjects == nil && error != nil)
            {
                NSLog(@"Error %@, %@", error, [error userInfo]);
                
             //   [[DataModel sharedObject] processError:error forErrorType:DatabaseError];
                
                foundObjects = @[];
            }
            
            return foundObjects;
        }
        @catch (NSException *exception)
        {
            return @[];
        }

    }
}

- (void) deleteManagedObject:(NSManagedObject *)object
{
    //Always delete object using the id incase it was loaded on a different thread
    [self deleteManagedObjectWithId:object.objectID];
}

- (void) deleteOrderedManagedObjects:(NSOrderedSet *) objects
{
    for (NSManagedObject *object in objects)
    {
        [self deleteManagedObject:object];
    }
}

- (void) deleteManagedObjects:(NSSet *) objects
{
    for (NSManagedObject *object in objects)
    {
        [self deleteManagedObject:object];
    }
}

- (void) deleteAllObjectsForEntityName:(NSString*)entityName
{
    NSArray* foundObjects = [self allObjectsForEntityName:entityName];
    
    if (foundObjects != nil)
    {
        for (NSManagedObject* managedObject in foundObjects)
        {
            [self deleteManagedObject:managedObject];
        }
    }
}

- (void) deleteAllObjectsForEntityName:(NSString*)entityName withPredicate:(NSPredicate *) predicate
{
    NSArray* foundObjects = [self allObjectsForEntityName:entityName withPredicate:predicate];
    
    if (foundObjects != nil)
    {
        for (NSManagedObject* managedObject in foundObjects)
        {
            [self deleteManagedObject:managedObject];
        }
    }
}


- (void) deleteManagedObjectWithId:(NSManagedObjectID *) objectId
{
    @synchronized(self)
    {
        @try
        {
            NSManagedObject *object =  [self objectWithID:objectId];
            
            [self deleteObject:object];
        }
        @catch (NSException *exception)
        {
            
        }
    }
}


+(NSManagedObject*)cloneAttributes:(NSManagedObject *)source
{
    NSString *entityName        = [[source entity] name];
    
    //create new object in data store
    NSManagedObject *cloned     = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:DATA_MODEL_CONTEXT.newBackgroundContext];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes    = [[NSEntityDescription entityForName:entityName inManagedObjectContext:DATA_MODEL_CONTEXT.newBackgroundContext] attributesByName];
    
    for (NSString *attr in attributes)
    {
        [cloned setValue:[source valueForKey:attr] forKey:attr];
    }
    return cloned;
}


+(NSManagedObject *) clone:(NSManagedObject *)source
{
    NSString *entityName                = [[source entity] name];
    
    //create new object in data store
    NSManagedObject *cloned             = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:DATA_MODEL_CONTEXT.newBackgroundContext];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes            = [[NSEntityDescription entityForName:entityName inManagedObjectContext:DATA_MODEL_CONTEXT.newBackgroundContext] attributesByName];
    
    for (NSString *attr in attributes)
    {
        [cloned setValue:[source valueForKey:attr] forKey:attr];
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships         = [[NSEntityDescription entityForName:entityName inManagedObjectContext:DATA_MODEL_CONTEXT.newBackgroundContext] relationshipsByName];
    
    for (NSString *relName in [relationships allKeys])
    {
        NSRelationshipDescription *rel  = [relationships objectForKey:relName];
        
        NSString *keyName               = [NSString stringWithFormat:@"%@",rel];
        
        if ([rel isToMany])
        {
            if (rel.isOrdered)
            {
                //get a set of all objects in the relationship
                NSMutableOrderedSet *sourceSet      = [source mutableOrderedSetValueForKey:keyName];
                NSMutableOrderedSet *clonedSet      = [cloned mutableOrderedSetValueForKey:keyName];
                NSEnumerator *e                     = [sourceSet objectEnumerator];
                
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [self clone:relatedObject];
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
            else
            {
                //get a set of all objects in the relationship
                NSMutableSet *sourceSet     = [source mutableSetValueForKey:keyName];
                NSMutableSet *clonedSet     = [cloned mutableSetValueForKey:keyName];
                NSEnumerator *e             = [sourceSet objectEnumerator];
                
                NSManagedObject *relatedObject;
                
                while ( relatedObject = [e nextObject])
                {
                    //Clone it, and add clone to set
                    NSManagedObject *clonedRelatedObject = [self clone:relatedObject];
                    [clonedSet addObject:clonedRelatedObject];
                }
            }
        }
        else
        {
            [cloned setValue:[source valueForKey:keyName] forKey:keyName];
        }
        
    }
    
    return cloned;
}

@end
