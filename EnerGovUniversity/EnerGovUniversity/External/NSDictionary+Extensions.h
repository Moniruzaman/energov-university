//
//  NSDictionary+Extensions.h
//  iG_Inspect
//
//  Created by Keith Dougherty on 2/16/17.
//  Copyright © 2017 Hollance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Extensions)

- (id)objectForCaseInsensitiveKey:(NSString *)key;

@end
