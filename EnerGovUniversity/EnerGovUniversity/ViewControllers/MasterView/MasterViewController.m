//
//  MasterViewController.m
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/22/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "MasterViewController.h"
#import "MBProgressHUD.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

@synthesize expandTableView, dataModelArray;
@synthesize rootCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[self initializeSampleDataModel];
    self.splitViewController.delegate = self;
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
 
   // self.expandTableView.allowsMultipleSelection = NO;
    [self.expandTableView setBackgroundColor:[UIColor clearColor]];

    [self setTitle:@"Energov University"];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:56.0f/255.0f
                                                                  green:72.0f/255.0f
                                                                   blue:132.0f/255.0f
                                                                  alpha:1.0f]];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(populateDataModelArray) name:@"KsyncAllData" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateDataModelArray
{
    self.dataModelArray = [[NSMutableArray alloc] init];
    rootCategory = [Category getRootCategories];
    self.dataModelArray  = [rootCategory getAllSubFolders];
    
    [self.expandTableView setDataSourceDelegate:self];
    [self.expandTableView setTableViewDelegate:self];
    
    [self.expandTableView reloadData];
}

#pragma mark - JKExpandTableViewDelegate
// return YES if more than one child under this parent can be selected at the same time.  Otherwise, return NO.
// it is permissible to have a mix of multi-selectables and non-multi-selectables.
- (BOOL) shouldSupportMultipleSelectableChildrenAtParentIndex:(NSInteger) parentIndex {
    /*
    if ((parentIndex % 2) == 0) {
        return NO;
    } else {
        return YES;
    }
    */
    return NO;
}

- (void) tableView:(UITableView *)tableView didSelectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex {
    //[[self.dataModelArray objectAtIndex:parentIndex] setObject:[NSNumber numberWithBool:YES] atIndex:childIndex];
    //NSLog(@"data array: %@", self.dataModelArray);
    self.selectedCategory = [self getChildObjectForParentIndex:parentIndex childIndex:childIndex];
    if ([self.selectedCategory isKindOfClass:[Category class]]) {
        [self performSegueWithIdentifier:@"showdetail" sender:nil];
    }
    
}

- (void) tableView:(UITableView *)tableView didDeselectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex {
    //[[self.dataModelArray objectAtIndex:parentIndex] setObject:[NSNumber numberWithBool:NO] atIndex:childIndex];
    //NSLog(@"data array: %@", self.dataModelArray);
}

- (UIFont *) fontForParents {
    return [UIFont fontWithName:@"Helvertica New" size:18];
}

- (UIFont *) fontForChildren {
    return [UIFont fontWithName:@"Helvertica New" size:18];
}

#pragma mark - JKExpandTableViewDataSource
- (NSInteger) numberOfParentCells
{
    return [self.dataModelArray count];
}

- (NSInteger) numberOfChildCellsUnderParentIndex:(NSInteger) parentIndex
{
    //NSMutableArray *childArray = [self.dataModelArray objectAtIndex:parentIndex];
    Category *parentCategory = [self.dataModelArray objectAtIndex:parentIndex];
    NSArray *childArray = [parentCategory getAllSubFolders];
    return [childArray count];
}

- (NSString *) labelForParentCellAtIndex:(NSInteger) parentIndex
{
    //return [NSString stringWithFormat:@"parent %ld", (long)parentIndex];
    return [self getParentObjectForIndex:parentIndex].name;
}

- (NSString *) labelForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex
{
    //return [NSString stringWithFormat:@"child %ld of parent %ld", (long)childIndex, (long)parentIndex];
    return [self getChildObjectForParentIndex:(NSInteger)parentIndex childIndex:childIndex].name;
}

- (BOOL) shouldDisplaySelectedStateForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex
{
    //NSMutableArray *childArray = [self.dataModelArray objectAtIndex:parentIndex];
   // return [[childArray objectAtIndex:childIndex] boolValue];
    return false;
}

- (UIImage *) iconForParentCellAtIndex:(NSInteger) parentIndex
{
    return [UIImage imageNamed:@"arrow-icon"];
}

- (UIImage *) iconForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex
{
   /*
    if (((childIndex + parentIndex) % 3) == 0) {
        return [UIImage imageNamed:@"folderIcon"];
    } else if ((childIndex % 2) == 0) {
        return [UIImage imageNamed:@"folderIcon"];
    } else {
        return [UIImage imageNamed:@"folderIcon"];
    }
    */
    return [UIImage imageNamed:@"folderIcon"];
}

- (BOOL) shouldRotateIconForParentOnToggle {
    return YES;
}

-(Category *)getParentObjectForIndex:(NSInteger) index
{
    return  [self.dataModelArray objectAtIndex:index];
}
-(Category *)getChildObjectForParentIndex:(NSInteger) index childIndex:(NSInteger) childIndex
{
    Category *parentCategory = [self getParentObjectForIndex:index];
    NSArray *childArray = [parentCategory getAllSubFolders];
    return  [childArray objectAtIndex:childIndex];
}


- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController
{
    if ([secondaryViewController isKindOfClass:[DetailsViewController class]])
    {
        // If our secondary controller is blank, do the collapse ourself by doing nothing.
        return NO;
    }
    
    // Otherwise, use the default behaviour.
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"showdetail"] && self.selectedCategory != nil)
    {
        // Get reference to the destination view controller
        UINavigationController *vc = [segue destinationViewController];
        
        NSArray *viewControllers = vc.viewControllers;
        for (UIViewController *destinationView in viewControllers)
        {
            if ([destinationView isKindOfClass:[DetailsViewController class]])
            {
                [(DetailsViewController *)destinationView setTitle:self.selectedCategory.name];
                NSArray *selectedFilesArray = [self.selectedCategory getAllFileObjects];
                [(DetailsViewController *)destinationView setDataModelArray:nil];
                [(DetailsViewController *)destinationView setDataModelArray:selectedFilesArray];
                [(DetailsViewController *)destinationView setDataOriginalDataModelArray:selectedFilesArray];
                break;
            }
        }
    }
}


- (IBAction)refreshDataAction:(id)sender
{
  //  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [GetFilesRequest getAllFilesWithResponseBlock:^(ServerRequest *request, ServerApiResult resultCode, id object)
     {
      // [MBProgressHUD hideHUDForView:self.view animated:YES];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"KsyncAllData" object:nil];
         NSLog(@"response");
         NSLog(@"response");
         if ([self.selectedCategory isKindOfClass:[Category class]]) {
             [self performSegueWithIdentifier:@"showdetail" sender:nil];
         }
     }];
}
@end
