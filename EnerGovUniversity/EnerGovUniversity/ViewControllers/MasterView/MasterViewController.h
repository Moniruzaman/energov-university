//
//  MasterViewController.h
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/22/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKExpandTableView.h"
#import "DetailsViewController.h"
#import "Category+CoreDataClass.h"
#import "File+CoreDataClass.h"
#import "GetFilesRequest.h"
#import "GetFileRequest.h"

@interface MasterViewController : UIViewController<JKExpandTableViewDelegate, JKExpandTableViewDataSource,UISplitViewControllerDelegate>

@property(nonatomic,weak) IBOutlet JKExpandTableView * expandTableView;

@property(nonatomic,strong) NSArray * dataModelArray;
@property(nonatomic,strong) Category *selectedCategory;
@property(nonatomic,strong) Category *rootCategory;
- (IBAction)refreshDataAction:(id)sender;

@end
