//
//  LoginViewController.h
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category+CoreDataClass.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)loginAction:(id)sender;

@end
