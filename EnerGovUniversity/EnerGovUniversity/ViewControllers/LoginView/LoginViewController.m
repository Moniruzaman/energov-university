//
//  LoginViewController.m
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "LoginViewController.h"

#import "GetFilesRequest.h"
#import "Category+CoreDataClass.h"
#import "GetFileRequest.h"

#import "Category+CoreDataClass.h"
#import "File+CoreDataClass.h"

@interface LoginViewController ()
    
@end

@implementation LoginViewController
@synthesize txtUsername;
@synthesize txtPassword;
    
- (void)viewDidLoad
{
    [super viewDidLoad];
    txtUsername.delegate = self;
    txtPassword.delegate = self;
}
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
    
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
    
- (IBAction)loginAction:(id)sender
{
    
    [GetFilesRequest getAllFilesWithResponseBlock:^(ServerRequest *request, ServerApiResult resultCode, id object)
    {
      [[NSNotificationCenter defaultCenter] postNotificationName:@"KsyncAllData" object:nil];
        NSLog(@"response");
        
    }];

}
    
#pragma mark - UITextFieldDelegate movements

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.txtUsername == textField)
    {
        [self.txtPassword becomeFirstResponder];
        
    } else
    {
        [self.txtUsername resignFirstResponder];
        [self.txtPassword resignFirstResponder];
    }
    return NO;
}
    
#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
         f.origin.y = -0.5f * keyboardSize.height;
        self.view.frame = f;
    }];
}
    
-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
@end

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
