//
//  DetailViewCellTableViewCell.h
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewCellTableViewCell;
@protocol DetailViewCellTableViewCellDelegate <NSObject>

- (void)playVideoOrOpenPdfForIndexCell:(DetailViewCellTableViewCell *)cell;

@end


@interface DetailViewCellTableViewCell : UITableViewCell

@property (assign, nonatomic) id<DetailViewCellTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIImageView *cellThumbImage;
@property (weak, nonatomic) IBOutlet UILabel *lblFileTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFileDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnPayOrOpenPdf;

-(void)configureCellWithValue:(id)data withIndexPath:(NSIndexPath *) indexpath;

@end
