 //
//  DetailViewCellTableViewCell.m
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "DetailViewCellTableViewCell.h"
#import "File+CoreDataClass.h"

@implementation DetailViewCellTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
- (IBAction)playVideoOrOpenPdfAction:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(playVideoOrOpenPdfForIndexCell:)])
    {
        [self.delegate playVideoOrOpenPdfForIndexCell:self];
    }
}

-(void)configureCellWithValue:(File *)data withIndexPath:(NSIndexPath *) indexPath
{
    if ([data isKindOfClass:[File class]] && data != nil)
    {
        if ([data.fileExtension isEqualToString:@"pdf"])
        {
            [_cellThumbImage setImage:[UIImage imageNamed:@"pdf-thumbnail"]];
            [_btnPayOrOpenPdf setImage:[UIImage imageNamed:@"openPdf"] forState:UIControlStateNormal];
        }
        else if ([data.fileExtension isEqualToString:@"mp4"] || [data.fileExtension isEqualToString:@"mp3"] || [data.fileExtension isEqualToString:@"mpeg"])
        {
            [_cellThumbImage setImage:[UIImage imageNamed:@"video-thumbnail"]];
            [_btnPayOrOpenPdf setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        }
        
        _lblFileTitle.text = data.fileName;
        _lblFileDescription.text = [NSString stringWithFormat:@"Created Date: %@",data.createdDate];
    }
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}
@end
