//
//  DetailsViewController.m
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "DetailsViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

#import "GetFileRequest.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController
    
@synthesize searchBar;
@synthesize dataModelArray;
@synthesize dataOriginalDataModelArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.searchBar.delegate = self;
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:56.0f/255.0f
                                                                  green:72.0f/255.0f
                                                                   blue:132.0f/255.0f
                                                                  alpha:1.0f]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.navigationController.navigationBar.translucent = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDatasourceAndDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (self.dataModelArray == nil || self.dataModelArray.count <= 0)
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No data available";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView     = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    else
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        tableView.backgroundView = nil;
    }

    return numOfSections;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataModelArray count];
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell) {
        [self.tableView registerNib:[UINib nibWithNibName:@"DetailViewCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    cell.delegate = self;
    [cell configureCellWithValue:[self.dataModelArray objectAtIndex:indexPath.row] withIndexPath:indexPath];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Detail cell controller delegate

- (void)playVideoOrOpenPdfForIndexCell:(DetailViewCellTableViewCell *)cell
{
    NSIndexPath *indexPath                      = [self.tableView indexPathForCell:cell];
    NSLog(@"%d Tap to play or open pdf.",(int)indexPath.row);
    
    __block File *dataFile  = [self.dataModelArray objectAtIndex:indexPath.row];
    if (dataFile != nil)
    {
        if (![NSString isNullOrEmpty:dataFile.filePath])
        {
            if ([dataFile.fileExtension isEqualToString:@"pdf"]  || [dataFile.fileExtension isEqualToString:@"txt"])
            {
                [self openPdfWithFileName:dataFile];
            }
            else if ([dataFile.fileExtension isEqualToString:@"mp4"] || [dataFile.fileExtension isEqualToString:@"mp3"] || [dataFile.fileExtension isEqualToString:@"mpeg"])
            {
                NSURL *videoURL = [NSURL URLWithString:[dataFile filePath]];
                [self playVideoFromUrl:videoURL];
            }
        }
        else
        {
            [GetFileRequest getFile:dataFile withResponseBlock:^(ServerRequest *request, ServerApiResult resultCode, id object)
             {
                 if (resultCode == ServerApiResultSuccessful)
                 {
                     File *file = (File *) object;
                     if ([file.fileExtension isEqualToString:@"pdf"] || [file.fileExtension isEqualToString:@"txt"])
                     {
                         file.filePath = [file.filePath substringFromIndex:7];
                         [self openPdfWithFileName:file];
                         
                     }
                     else if ([file.fileExtension isEqualToString:@"mp4"] || [file.fileExtension isEqualToString:@"mp3"] || [file.fileExtension isEqualToString:@"mpeg"])
                     {
                         NSURL *videoURL = [NSURL URLWithString:[file filePath]];
                         [self playVideoFromUrl:videoURL];
                     }
                 }
             }];
            
        }
    }

}
#pragma mark - UISearchBarDelegate
    
- (void)searchBarCancelButtonClicked:(UISearchBar*)theSearchBar
{
    self.dataModelArray = self.dataOriginalDataModelArray;
    [self.tableView reloadData];
    
    [self.searchBar setText:@""];
    [theSearchBar resignFirstResponder];
}
    
- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    
}
    
- (void)searchBar:(UISearchBar*)theSearchBar textDidChange:(NSString*)searchText
{
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchText isEqualToString:@""])
    {
        self.dataModelArray = [[NSMutableArray alloc] initWithArray:[File searchForFileWithName:searchText]];
    }
    [self.tableView reloadData];
}
- (void)playVideoFromUrl:(NSURL  *)url{

    AVPlayerViewController * _moviePlayer1 = [[AVPlayerViewController alloc] init];
    _moviePlayer1.player = [AVPlayer playerWithURL:url];
    
    [self presentViewController:_moviePlayer1 animated:YES completion:^{
        [_moviePlayer1.player play];
    }];

}
- (void)openPdfWithFileName:(File *)file
{
    //Need to get file path from local path after download
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:file.filePath password:nil];
    if (document != nil)
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:readerViewController animated:YES completion:nil];
        
    }
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
