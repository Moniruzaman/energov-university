//
//  DetailsViewController.h
//  EnerGovUniversity
//
//  Created by SM Moniruzaman on 10/21/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewCellTableViewCell.h"
#import "ReaderDocument.h"
#import "ReaderViewController.h"
#import "File+CoreDataClass.h"

@interface DetailsViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate, UISearchBarDelegate,ReaderViewControllerDelegate,DetailViewCellTableViewCellDelegate>
    
@property (nonatomic, strong) IBOutlet UISearchBar   * searchBar;
@property(nonatomic,strong) NSArray * dataModelArray;
@property(nonatomic,strong) NSArray * dataOriginalDataModelArray;
@end
