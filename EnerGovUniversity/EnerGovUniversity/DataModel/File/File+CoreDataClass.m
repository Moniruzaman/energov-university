//
//  File+CoreDataClass.m
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import "File+CoreDataClass.h"
#import "NSDate+MHExtensions.h"

@implementation File

+ (NSArray *) searchForFileWithName:(NSString *) searchText
{
    NSArray *foundObjects           = [DATA_MODEL_CONTEXT.viewContext allObjectsForEntityName:NSStringFromClass([File class])
                                                                                     sortedBy:@"fileName"
                                                                                    ascending:NO
                                                                          withPredicateString:@"fileName CONTAINS[cd] %@", searchText];
    
    return foundObjects;
}

+ (File *) parseResponse:(NSDictionary *) response
{
    NSManagedObjectContext *context = DATA_MODEL_CONTEXT.viewContext;
    File *newFile                   = [context insertNewEntityForName:NSStringFromClass([File class])];
    
    newFile.isNew                   = YES;
    newFile.isDownloaded            = NO;
    newFile.isViewed                = NO;
    newFile.createdDate             = [NSDate date];
    
    id object = [response valueForKey:@"FileName"];
    if (![NSString isNullOrEmpty:object])
        newFile.fileName            = object;
    
    object = [response valueForKey:@"FileLength"];
    if (![NSString isNullOrEmpty:object])
        newFile.fileLength          = object;
    
    object = [response valueForKey:@"FileExtension"];
    if (![NSString isNullOrEmpty:object])
        newFile.fileExtension       = object;
    
    object = [response valueForKey:@"FilePath"];
    if (![NSString isNullOrEmpty:object])
        newFile.fileURL             = object;
    
    object = [response valueForKey:@"CreatedOn"];
    if (![NSString isNullOrEmpty:object])
        newFile.createdDate         = [NSDate dateFromString:(NSString *)object];

    newFile.createdDate = [NSDate date];
    
    [context saveContext];
    
    return newFile;
}


@end
