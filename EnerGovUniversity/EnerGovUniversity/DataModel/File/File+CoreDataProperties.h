//
//  File+CoreDataProperties.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import "File+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface File (CoreDataProperties)

+ (NSFetchRequest<File *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *createdDate;
@property (nullable, nonatomic, copy) NSString *fileExtension;
@property (nullable, nonatomic, copy) NSString *fileLength;
@property (nullable, nonatomic, copy) NSString *fileName;
@property (nullable, nonatomic, copy) NSString *filePath;
@property (nullable, nonatomic, copy) NSString *fileURL;
@property (nonatomic) BOOL isDownloaded;
@property (nonatomic) BOOL isNew;
@property (nonatomic) BOOL isViewed;
@property (nullable, nonatomic, retain) Category *category;

@end

NS_ASSUME_NONNULL_END
