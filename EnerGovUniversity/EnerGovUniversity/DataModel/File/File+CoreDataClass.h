//
//  File+CoreDataClass.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

NS_ASSUME_NONNULL_BEGIN

@interface File : NSManagedObject

+ (File *) parseResponse:(NSDictionary *) response;

+ (NSArray *) searchForFileWithName:(NSString *) searchText;

@end

NS_ASSUME_NONNULL_END

#import "File+CoreDataProperties.h"
