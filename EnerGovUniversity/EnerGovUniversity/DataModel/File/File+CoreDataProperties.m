//
//  File+CoreDataProperties.m
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import "File+CoreDataProperties.h"

@implementation File (CoreDataProperties)

+ (NSFetchRequest<File *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"File"];
}

@dynamic createdDate;
@dynamic fileExtension;
@dynamic fileLength;
@dynamic fileName;
@dynamic filePath;
@dynamic fileURL;
@dynamic isDownloaded;
@dynamic isNew;
@dynamic isViewed;
@dynamic category;

@end
