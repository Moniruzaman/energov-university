//
//  DataModel.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//


@interface DataModel : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

+ (DataModel *) sharedObject;

- (void)saveContext;

@end
