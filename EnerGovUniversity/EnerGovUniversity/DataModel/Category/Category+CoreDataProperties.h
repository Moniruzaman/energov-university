//
//  Category+CoreDataProperties.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import "Category+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Category (CoreDataProperties)

+ (NSFetchRequest<Category *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSSet<File *> *files;
@property (nullable, nonatomic, retain) NSSet<Category *> *subCategories;

@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addFilesObject:(File *)value;
- (void)removeFilesObject:(File *)value;
- (void)addFiles:(NSSet<File *> *)values;
- (void)removeFiles:(NSSet<File *> *)values;

- (void)addSubCategoriesObject:(Category *)value;
- (void)removeSubCategoriesObject:(Category *)value;
- (void)addSubCategories:(NSSet<Category *> *)values;
- (void)removeSubCategories:(NSSet<Category *> *)values;

@end

NS_ASSUME_NONNULL_END
