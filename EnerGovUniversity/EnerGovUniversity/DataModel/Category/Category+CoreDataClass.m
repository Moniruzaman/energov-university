//
//  Category+CoreDataClass.m
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import "Category+CoreDataClass.h"
#import "File+CoreDataClass.h"

@implementation Category
@synthesize sortedFiles;

+ (Category *) getCategoryWithName:(NSString *)category
{
    NSArray *foundObjects  = [DATA_MODEL_CONTEXT.viewContext allObjectsForEntityName:NSStringFromClass([Category class])
                                                                           withPredicateString:@"name == %@", category];
    
    if (foundObjects.count > 0)
        return foundObjects.firstObject;
    else
        return nil;
}

+ (Category *) getRootCategories
{
    NSArray *foundObjects  = [DATA_MODEL_CONTEXT.viewContext allObjectsForEntityName:NSStringFromClass([Category class])  sortedBy:@"name" ascending:NO withPredicateString:@"name == %@", @"Root"];
    
    if (foundObjects.count > 0)
        return foundObjects.firstObject;
    else
        return nil;
}

+ (void) deleteAllCategories
{
    NSManagedObjectContext *context = DATA_MODEL_CONTEXT.viewContext;
    
    [context deleteAllObjectsForEntityName:NSStringFromClass([Category class])];

    [context saveContext];
}

+ (Category *) parseDictionary:(NSDictionary *) dictionary
{
    NSManagedObjectContext *context = DATA_MODEL_CONTEXT.viewContext;
    
    Category *category              = [context insertNewEntityForName:NSStringFromClass([Category class])];
    
    id object = [dictionary valueForKey:@"FolderName"];
    if (![NSString isNullOrEmpty:object])
        category.name = object;
    else
       category.name = @"Root";

    object = [dictionary valueForKey:@"SubFolders"];
    if (object != nil && [object isKindOfClass:[NSArray class]])
    {
        NSArray *subFolders = (NSArray *) object;
        for (NSDictionary *subDictionary in subFolders)
        {
            Category *subCategory = [Category parseDictionary:subDictionary];
            
            if (subCategory && ![subCategory.name isEqualToString:@"Root"])
                [category addSubCategoriesObject:subCategory];
        }
    }
    
    object = [dictionary valueForKey:@"Files"];
    if (object != nil && [object isKindOfClass:[NSArray class]])
    {
        NSArray *files = (NSArray *) object;
        
        for (NSDictionary *fileDictionary in files)
        {
            File *file = [File parseResponse:fileDictionary];
            
            if (file)
                [category addFilesObject:file];
        }
    }
    
    [context saveContext];
    
    return category;
}
+ (NSArray * _Nullable) getAACategories
{
    NSArray *foundObjects  = [DATA_MODEL_CONTEXT.viewContext allObjectsForEntityName:NSStringFromClass([Category class])
                              ];
    
    if (foundObjects.count > 0)
        return foundObjects.firstObject;
    else
        return nil;
}
- (NSArray *) getAllFileObjects
{
    NSSortDescriptor* indexSortDescriptor       = [NSSortDescriptor sortDescriptorWithKey:@"fileName" ascending:YES];
    NSArray* sortedFiles                        = [[self.files allObjects]sortedArrayUsingDescriptors:[NSArray arrayWithObjects: indexSortDescriptor, nil]];
    return sortedFiles.count >0 ? sortedFiles : [[NSArray alloc] init];
}
- (NSArray *) getAllSubFolders
{
    NSSortDescriptor* indexSortDescriptor       = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray* sortedFolders                        = [[self.subCategories allObjects]sortedArrayUsingDescriptors:[NSArray arrayWithObjects: indexSortDescriptor, nil]];
    return sortedFolders.count >0 ? sortedFolders : [[NSArray alloc] init];
}
@end
