//
//  Category+CoreDataClass.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "File+CoreDataClass.h"

@class File;

NS_ASSUME_NONNULL_BEGIN

@interface Category : NSManagedObject
@property (nonatomic, strong) NSMutableArray* sortedFiles;

+ (Category *) createNewCategoryWithName:(NSString *) name;
- (NSArray *) getAllFileObjects;
@end

NS_ASSUME_NONNULL_END

#import "Category+CoreDataProperties.h"
