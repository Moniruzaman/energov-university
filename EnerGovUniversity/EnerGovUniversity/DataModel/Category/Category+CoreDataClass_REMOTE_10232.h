//
//  Category+CoreDataClass.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class File;

NS_ASSUME_NONNULL_BEGIN

@interface Category : NSManagedObject

+ (Category *) createNewCategoryWithName:(NSString *) name;

+ (Category * _Nullable) getCategoryWithName:(NSString *) category;

+ (void) deleteAllCategories;

+ (Category *) parseDictionary:(NSDictionary *) dictionary;

@end

NS_ASSUME_NONNULL_END

#import "Category+CoreDataProperties.h"
