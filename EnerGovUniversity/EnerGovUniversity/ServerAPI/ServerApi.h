/*!
 * \file ServerApi.h
 *
 * Classes for communicating with the server API.
 */

//#import "NetworkController.h"

#import "NetworkOperationManager.h"


@class ServerRequest;

/*!
 * The block type for the API calls.
 *
 * @param resultCode one of the ServerApiResult constants
 * @param object this can be the object in question, an NSError object, or nil
 */
typedef void (^ServerApiBlock)(ServerRequest * _Nonnull request, ServerApiResult resultCode, NSObject * _Nullable object);


/*!
 * Base class for all server requests.
 *
 * Most requests will directly update the data model with the newly received data.
 */
@interface ServerRequest : NSObject


/*! The block that will be notified with the results of the operation. */
@property (nonatomic, copy)         ServerApiBlock  _Nonnull  block;
@property (nonatomic, readwrite) __block  NSInteger unauthorizedRefreshCounter;
@property (nonatomic, readwrite) __block  NSInteger connectionLostRetryCounter;


//@property (nonatomic, copy)         NSURLSessionDataTask    *ptrRequestTask;
//
//
//@property (nonatomic, retain)       NSMutableData           *dataToDownload;
//@property (nonatomic, readwrite)    float                   downloadSize;
//@property (nonatomic, readwrite)    float                   progress;

- (NSObject * _Nonnull) initWithResponseBlock:(ServerApiBlock _Nonnull) responseBlock;


- (void) performGetOperationForURL:(NSString * _Nonnull) urlString;
- (void) performGetOperationForURL:(NSString * _Nonnull)urlString withProgressBlock:(void (^ _Nullable)(NSProgress * _Nonnull))downloadProgress;

- (void) performDownloadOperationForURL:(NSString * _Nonnull)urlString withProgressBlock:(void (^ _Nullable)(NSProgress * _Nonnull))downloadProgress;

- (void) handleSuccessfulResponse:(NSObject * _Nonnull) responseObject;

/*!
 * Subclasses should override this to deal with the response.
 *
 * @param dictionary the contents of the response
 */
- (void)handleResponseDictionary:(NSDictionary * _Nonnull)dictionary;

/*!
 * Subclasses should override this to deal with the response.
 *
 * @param array the contents of the response
 */
- (void)handleResponseArray:(NSArray * _Nonnull)array;

/*!
 * Subclasses should override this to deal with the response.
 *
 * @param object the contents of the response
 */
- (void)handleResponseObject:(NSObject * _Nonnull)object;


- (void) handleNetworkError:(NSError * _Nonnull) error withResponse:(NSHTTPURLResponse * _Nullable) response;

- (void) handleNetworkError:(NSError * _Nonnull) error;

@end
