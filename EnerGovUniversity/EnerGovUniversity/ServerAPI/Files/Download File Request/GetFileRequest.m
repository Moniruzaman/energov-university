//
//  getFile.m
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "GetFileRequest.h"
#import "File+CoreDataClass.h"

@interface GetFileRequest ()

@property (nonatomic, nonnull, strong) File *file;

@end


@implementation GetFileRequest

+ (void) getFile:(File *) file withResponseBlock:(ServerApiBlock)block
{
    GetFileRequest *request             = [[GetFileRequest alloc] initWithResponseBlock:block];
    request.file                        = file;

    NSString* urlString                 = [NSString stringWithFormat:@"files/display/%@/%@", file.fileName, file.fileExtension];
    
    [request performDownloadOperationForURL:urlString withProgressBlock:nil];
}

#pragma mark - Downloading Inspection Data
- (void) handleResponseObject:(NSObject *)object
{
    File *file      = [DATA_MODEL_CONTEXT.viewContext objectWithID:self.file.objectID];
    
    NSURL *url      = (NSURL *) object;
    
    file.filePath   =  [url absoluteString];
    
    [DATA_MODEL_CONTEXT.viewContext saveContext];
    
    self.block(self, ServerApiResultSuccessful, file);

}

- (void) handleNetworkError:(NSError *) error
{
    if (error.code == NSURLErrorNotConnectedToInternet)
        self.block(self, ServerApiResultNotConnected, error);
    else
        self.block(self, ServerApiResultNetworkError, error);
}

@end
