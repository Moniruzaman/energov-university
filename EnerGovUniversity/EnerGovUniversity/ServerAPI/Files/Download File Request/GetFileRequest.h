//
//  getFile.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "ServerApi.h"

@class File;

@interface GetFileRequest : ServerRequest

+ (void) getFile:(File *) file withResponseBlock:(ServerApiBlock)block;

@end
