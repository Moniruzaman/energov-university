//
//  GetFilesRequest.h
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "ServerApi.h"

@interface GetFilesRequest : ServerRequest

+ (void) getAllFilesWithResponseBlock:(ServerApiBlock)block;

- (void) handleResponseDictionary:(NSDictionary*)dictionary;

@end
