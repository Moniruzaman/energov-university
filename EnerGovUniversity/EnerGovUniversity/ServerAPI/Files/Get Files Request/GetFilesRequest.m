//
//  GetFilesRequest.m
//  EnerGovUniversity
//
//  Created by Keith Dougherty on 10/25/17.
//  Copyright © 2017 Tylertech. All rights reserved.
//

#import "GetFilesRequest.h"

#import "Category+CoreDataClass.h"

@implementation GetFilesRequest

+ (void) getAllFilesWithResponseBlock:(ServerApiBlock)block
{
    GetFilesRequest *request         = [[GetFilesRequest alloc] initWithResponseBlock:block];
    
    NSString* urlString              = [NSString stringWithFormat:@"files/all"];
        
    [request performGetOperationForURL:urlString];
}

#pragma mark - Downloading Inspection Data

- (void) handleResponseDictionary:(NSDictionary*)dictionary
{
    [Category deleteAllCategories];
    
    id object = [dictionary valueForKey:@"IsSuccessfull"];
    if (object != nil && [object isKindOfClass:[NSNumber class]])
    {
        if ([(NSNumber *)object boolValue])
        {
            object = [dictionary valueForKey:@"Folders"];
            if (object != nil && [object isKindOfClass:[NSArray class]])
            {
                for (NSDictionary *category in (NSArray *)object)
                {
                    [Category parseDictionary:category];
                }
            }
            
            self.block(self, ServerApiResultSuccessful, nil);
        }
        else
        {
            object = [dictionary valueForKey:@"ErrorMessage"];
            if (![NSString isNullOrEmpty:object])
                self.block(self, ServerApiResultRequestFailed, object);
            else
                self.block(self, ServerApiResultRequestFailed, nil);
        }
    }
    else
    {
        self.block(self, ServerApiResultBadResponse, nil);
    }
}


- (void) handleNetworkError:(NSError *) error
{
    if (error.code == NSURLErrorNotConnectedToInternet)
        self.block(self, ServerApiResultNotConnected, error);
    else
        self.block(self, ServerApiResultNetworkError, error);
}


@end
