
#import "ServerApi.h"
#import "HTTPStatusCodes.h"


@implementation ServerRequest
{
    NetworkOperationManager *_networkManager;
}

@synthesize block;


- (id) initWithResponseBlock:(ServerApiBlock) responseBlock
{
    self = [super init];
    
    if (self)
    {
        self.block                              = responseBlock;
        _networkManager                         = [NetworkOperationManager sharedManager];
    }
    
    return self;
}

#pragma mark - Generic Requests

- (void) performGetOperationForURL:(NSString*)urlString
{
    [self performGetOperationForURL:urlString withProgressBlock:nil];
}
    
- (void) performGetOperationForURL:(NSString * _Nonnull)urlString withProgressBlock:(void (^ _Nullable)(NSProgress * _Nonnull))downloadProgress

{
    @try
    {

#if DEBUG
            NSLog(@"REQUEST URL: %@", urlString);
#endif
        
        if (_networkManager.internetReachability.isReachable)
        {
            [_networkManager GetFromURL:urlString progress:^(NSProgress *progress)
             {
                 if (downloadProgress)
                 {
                     downloadProgress(progress);
                 }
             }
                                success:^(id responseObject)
             {
                 [self handleSuccessfulResponse:responseObject];
             }
                                failure:^(NSURLResponse *response, NSError *error)
             {
                 NSHTTPURLResponse *httpResponse    = (NSHTTPURLResponse*)response;
    #if DEBUG
                 NSLog(@"Error: %@", error);
    #endif
                 
                 [self handleNetworkError:error withResponse:httpResponse];
             }];
        }
        else
        {
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
            
            [self handleNetworkError:error withResponse:nil];
        }
    }
    @catch (NSException *exception)
    {
      
    }
}

- (void) performDownloadOperationForURL:(NSString * _Nonnull)urlString withProgressBlock:(void (^ _Nullable)(NSProgress * _Nonnull))downloadProgress

{
    @try
    {
        
#if DEBUG
        NSLog(@"REQUEST URL: %@", urlString);
#endif
        
        if (_networkManager.internetReachability.isReachable)
        {
            [_networkManager DownloadFromURL:urlString
                                    progress:^(NSProgress *progress)
             {
                 if (downloadProgress)
                 {
                     downloadProgress(progress);
                 }
             }
                                success:^(id responseObject)
             {
                 [self handleSuccessfulResponse:responseObject];
             }
                                failure:^(NSURLResponse *response, NSError *error)
             {
                 NSHTTPURLResponse *httpResponse    = (NSHTTPURLResponse*)response;
#if DEBUG
                 NSLog(@"Error: %@", error);
#endif
                 
                 [self handleNetworkError:error withResponse:httpResponse];
             }];
        }
        else
        {
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
            
            [self handleNetworkError:error withResponse:nil];
        }
    }
    @catch (NSException *exception)
    {
        
    }
}


- (void) dealloc
{
#if DEBUG_REQUEST_STATUS
    
	NSLog(@"dealloc %@", self);
    
#endif
}

#pragma mark - Private Methods




#pragma mark - 


- (void) handleSuccessfulResponse:(id) responseObject
{
    if ([responseObject isKindOfClass:[NSDictionary class]])
        [self handleResponseDictionary:(NSDictionary*)responseObject];
    else if ([responseObject isKindOfClass:[NSArray class]])
        [self handleResponseArray:(NSArray*)responseObject];
    else
        [self handleResponseObject:responseObject];
}

- (void) handleResponseDictionary:(NSDictionary*)dictionary
{
    // This will only be called if the Request class does not implement the response,
    // which would mean an unexpected response was recieved
    self.block(self, ServerApiResultBadResponse, nil);
}

- (void) handleResponseArray:(NSArray*)array
{
    // This will only be called if the Request class does not implement the response,
    // which would mean an unexpected response was recieved
    self.block(self, ServerApiResultBadResponse, nil);
}

- (void) handleResponseObject:(id)object
{
	// This will only be called if the Request class does not implement the response,
    // which would mean an unexpected response was recieved
    self.block(self, ServerApiResultBadResponse, nil);
}

- (void) handleNetworkError:(NSError *)error
{
    if (error.code == NSURLErrorNotConnectedToInternet)
    {
        self.block(self, ServerApiResultNotConnected, error);
    }
    else
    {
        self.block(self, ServerApiResultNetworkError, error);
    }
}

- (void) handleNetworkError:(NSError *) error withResponse:(NSHTTPURLResponse *) response
{
    [self handleNetworkError:error];
}



@end
