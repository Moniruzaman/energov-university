//
//  NetworkOperationManager.h
//  iG_Inspect
//
//  Created by Keith Dougherty on 11/11/15.
//  Copyright © 2015 Hollance. All rights reserved.
//

#import "AFNetworking.h"

#import "Reachability.h"

/*!
 * Possible error codes for the server API operations.
 */
typedef enum
{
    ServerApiResultSuccessful = 0,                  ///< everything went OK
    ServerApiResultRequestFailed,
    ServerApiResultBadResponse,                     ///< the GET or POST request we sent was considered invalid by the server
    ServerApiResultNetworkError,
    ServerApiResultNotConnected,
    ServerApiResultServerError,
    ServerApiResultLicenseInvalid,                  ///< we sent an invalid license key
    ServerApiResultUserNotAuthenticated,            ///< we sent a bad user ID to processtoken
    ServerApiResultProcessTokenFailed,
    
    ServerApiResultFileError,                       ///< could not save attachment into a file
    ServerAPiResultUnknownError                     ///< error received is not currently being tracked
}
ServerApiResult;

NS_ASSUME_NONNULL_BEGIN

@interface NetworkOperationManager : AFHTTPSessionManager
    
/*
 * The completion handler that must be called when background session is done
 */
@property (nonatomic, copy) void (^ _Nullable savedCompletionHandler)(void);

+ (NetworkOperationManager * _Nonnull) sharedManager;

@property (nonatomic, retain, nonnull) Reachability *internetReachability;

/**
 * Attachments will be uploaded when the app is active and will continue into the background if the app
 * went in to background.
 */
- (void) GetFromURL:(NSString * _Nonnull) urlString
           progress:(void (^ _Nullable)(NSProgress * _Nonnull))downloadProgress
            success:(void (^ _Nonnull)(id _Nullable))success
            failure:(void (^ _Nonnull)(NSURLResponse * _Nullable, NSError * _Nonnull))failure;

- (void) DownloadFromURL:(NSString *) urlString
                progress:(void (^)(NSProgress * _Nonnull))progress
                 success:(void (^)(id _Nullable))success
                 failure:(void (^)(NSURLResponse * _Nullable, NSError * _Nonnull))failure;

@end

NS_ASSUME_NONNULL_END
