//
//  NetworkOperationManager.m
//  iG_Inspect
//
//  Created by Keith Dougherty on 11/11/15.
//  Copyright © 2015 Hollance. All rights reserved.
//

#import "NetworkOperationManager.h"
#import "MHFileUtil.h"

#import <MobileCoreServices/MobileCoreServices.h>    


@implementation NetworkOperationManager

+ (NetworkOperationManager *) sharedManager
{
    static NetworkOperationManager *objNetworkOperationManager  = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        objNetworkOperationManager                              = [[NetworkOperationManager alloc] init];
    });
    
    return objNetworkOperationManager;
}

- (instancetype) init
{
    NSURLSessionConfiguration *configuration                = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    //The timeout interval to use when waiting for additional data.
    configuration.timeoutIntervalForRequest                 = 60 * 3;
    
    //Keep a request on the network for up to 1 hour before timing out the request to allow it to re-attempt later
    configuration.timeoutIntervalForResource                = 60 * 3;
    
    //A Boolean value that determines whether connections should be made over a cellular network.
    configuration.allowsCellularAccess                      = YES;
    configuration.requestCachePolicy                        = NSURLCacheStorageNotAllowed;
    
//    configuration.HTTPShouldSetCookies                      = YES;
//    configuration.HTTPCookieStorage                         = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    configuration.HTTPCookieAcceptPolicy                    = NSHTTPCookieAcceptPolicyAlways;
//
//    configuration.waitsForConnectivity                      = NO;
//    configuration.multipathServiceType                      = NSURLSessionMultipathServiceTypeAggregate;
    
    if (self = [super initWithSessionConfiguration:configuration])
    {
        //Set the Request Serializer to format all request into JSON
        AFJSONRequestSerializer *requestSerializer              = [AFJSONRequestSerializer  serializer];
        requestSerializer.cachePolicy                           = NSURLRequestReloadIgnoringLocalCacheData;
        requestSerializer.timeoutInterval                       = 180.0;

        AFJSONResponseSerializer *responseSerializer            = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        [self setRequestSerializer:requestSerializer];
        [self setResponseSerializer:responseSerializer];
        
        //Optionally add a queue to have responses operate on a asynchronous thread
        self.completionQueue                                    = nil;
        
        self.attemptsToRecreateUploadTasksForBackgroundSessions = YES;

        // when entire background session done, call completion handler
        [self configureUploadDownloadFinished];
        [self configureBackgroundSessionFinished];
        
        self.internetReachability                               = [Reachability reachabilityForInternetConnection];
        [self.internetReachability startNotifier];
        
#ifdef TEST_FLIGHT
        self.securityPolicy.validatesDomainName                 = NO;
#endif
        
    }
    
    return self;
}

- (NSURL *) baseURL
{
    return [NSURL URLWithString:@"https://duldplthhannan.corp.tylertechnologies.com/TylerUniversity/api/"];
}

#pragma mark - Background activities


- (void) GetFromURL:(NSString *) urlString
           progress:(void (^)(NSProgress * _Nonnull))progress
            success:(void (^)(id _Nullable))success
                 failure:(void (^)(NSURLResponse * _Nullable, NSError * _Nonnull))failure
{
    NSLog(@"GET FROM URL- %@", urlString);
    
    [self GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress)
    {
         if (progress)
         {
             progress(downloadProgress);
         }
    }
                                   success:^(NSURLSessionDataTask *task, id responseObject)
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        
        NSLog(@"background Success Headers:  %@", httpResponse);
        
        if (success)
        {
            success(responseObject);
        }
    }
                                   failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        NSLog(@"background error -- %@", error);
        
        if (failure)
        {
            failure(task.response, error);
        }
    }];
}

- (void) DownloadFromURL:(NSString *) urlString
           progress:(void (^)(NSProgress * _Nonnull))progress
            success:(void (^)(id _Nullable))success
            failure:(void (^)(NSURLResponse * _Nullable, NSError * _Nonnull))failure
{
    NSLog(@"GET FROM URL- %@", urlString);
    
    urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET"
                                                                   URLString:[[NSURL URLWithString:urlString relativeToURL:self.baseURL] absoluteString]
                                                                  parameters:nil
                                                                       error:&serializationError];
    if (serializationError)
    {
        if (failure)
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
            dispatch_async(self.completionQueue ?: dispatch_get_main_queue(), ^{
                failure(nil, serializationError);
            });
#pragma clang diagnostic pop
        }
        
        return;
    }

   __block NSURLSessionDownloadTask *downloadTask = [self downloadTaskWithRequest:request
                                                                  progress:^(NSProgress * _Nonnull downloadProgress)
                                              {
                                                  if (progress)
                                                  {
                                                      progress(downloadProgress);
                                                  }
                                              }
                                                               destination:^NSURL *(NSURL *targetPath, NSURLResponse *response)
    {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    }
                                                         completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error)
    {
        NSLog(@"File downloaded to: %@", filePath);
        
        if (error)
        {
            if (failure)
            {
                failure(response, error);
            }
        }
        else
        {
            if (success)
            {
                success(filePath);
            }
        }
        
    }];
    
    [downloadTask resume];
}



- (void)configureUploadDownloadFinished
{
    [self setTaskDidSendBodyDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionTask * _Nonnull task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend)
     {
         NSLog(@"setTaskDidSendBodyDataBlock :: %lld, totalToSent: %lld, expectedToSend: %lld", bytesSent, totalBytesSent, totalBytesExpectedToSend);
         // not doing anything for now, may be used in the future
     }];
    
    [self setDownloadTaskDidFinishDownloadingBlock:^NSURL *(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, NSURL *location)
     {
         // not doing anything for now, may be used in the future
         NSLog(@"setDownloadTaskDidFinishDownloadingBlock");
         return nil;
     }];
    
    [self setTaskDidCompleteBlock:^(NSURLSession *session, NSURLSessionTask *task, NSError *error)
     {
         if (error)
         {
             // handle error here, e.g.,
             NSLog(@"setTaskDidCompleteBlock :: task -- %@ -- didCompleteError: %@: %@", task.taskDescription, [task.originalRequest.URL lastPathComponent], error);
         }
         else
         {
             NSLog(@"setTaskDidCompleteBlock :: task finished: %@", task.taskDescription);
         }
     }];
}

- (void)configureBackgroundSessionFinished
{
    // the entire session's work is finished, call completion handler
    __weak __typeof(self) weakSelf = self;
    
    [self setDidFinishEventsForBackgroundURLSessionBlock:^(NSURLSession *session)
    {
        if (weakSelf.savedCompletionHandler)
        {
            // this is not going to get called unless the app is in the background
            NSLog(@"setDidFinishEventsForBackgroundURLSessionBlock :: called completion handler: taskName: %@", session.description);
            

            
            weakSelf.savedCompletionHandler();
            weakSelf.savedCompletionHandler = nil;
        }
    }];
}

#pragma mark - Override Methods
    
- (NSURLSessionDataTask *) dataTaskWithRequest:(NSURLRequest *)mutableRequest
                                uploadProgress:(nullable void (^)(NSProgress * _Nonnull))uploadProgressBlock
                              downloadProgress:(nullable void (^)(NSProgress * _Nonnull))downloadProgressBlock
                             completionHandler:(nullable void (^)(NSURLResponse * _Nonnull, id _Nullable, NSError * _Nullable))completionHandler
{
#ifdef DEBUG
    NSLog(@"REQUEST URL: %@",       mutableRequest.URL.absoluteString);
    NSLog(@"REQUEST HEADERS: %@",   mutableRequest.allHTTPHeaderFields);
    NSLog(@"REQUEST TIMEOUT: %f",   mutableRequest.timeoutInterval);
#endif
    
    return [super dataTaskWithRequest:mutableRequest
                       uploadProgress:uploadProgressBlock
                     downloadProgress:downloadProgressBlock
                    completionHandler:completionHandler];
}

@end
